const express = require('express');
const router = express.Router();
var { Todos, Users} = require("../database/schema/index");

router.get('/', async (req, res, next) => {
  try {
    let todos = await Todos.find({createdBy: req.user.id});
    res.render('index', { todos });
  } catch (error) {
    next();
  }
});

router.post('/', async (req, res, next) => {
  try {
    let activity = req.body.activity;

    let userId = req.user.id;

    let todo = {
      activity: activity,
      status: false,
      createdBy: userId
    }

    await Todos.create(todo);

    return res.redirect('/');
  } catch (error) {
    next();
  }
});

// Delete 
router.post('/delete', async (req, res, next) => {
  try {
    let id = req.body.id;
    let deletedTodo = await Todos.findById(id);
    deletedTodo.delete();
    return res.redirect('/');
  } catch (error) {
    next();
  }
});

// Change status => done.
router.post('/done', async (req, res, next) => {
  try {
    let id = req.body.id;
    console.log(id);
    let doneTodo = await Todos.findById(id);
    doneTodo.status = true;
    doneTodo.save();
    
    return res.redirect('/?done=true');
  } catch (error) {
    next();
  }
});

module.exports = router;