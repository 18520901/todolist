const express = require('express');
const path = require('path');
const { Todos, Users} = require("./database/schema/index");
let connectDB = require("./database/connect");
const todoRouter = require('./routes/todoRouter');

connectDB.connect();

var app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', todoRouter);

app.listen('8080', (req, res, error) => {
  if (!error) {
    console.log('working');
  }
})